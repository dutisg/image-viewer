# Downloads 32 images covering 360 view for testing purposes
# Example endpoint returning frame by num:
# - https://content.cylindo.com/api/v2/4404/products/ARCHIBALDCHAIR/frames/1/

API_URL="https://content.cylindo.com/api/v2/4404/products/ARCHIBALDCHAIR/frames"

DELAY_MIN=0.1
DELAY_MAX=0.6
OUTPUT_DIR=public/test-frames

# create dir if not there yet
mkdir -p $OUTPUT_DIR

for i in {1..32}; do
  curl $API_URL/$i/ -o ./$OUTPUT_DIR/frame-$i.jpg
  DELAY=`jot -r 1  $DELAY_MIN $DELAY_MAX`
  sleep $DELAY # Definitely not a robot
done
