
## API

docs: https://learn.cylindo.com/hc/en-us/articles/360005334798-API 
example: https://content.cylindo.com/api/v2/4404/products/ARCHIBALDCHAIR/frames/1/ 

Key takeaways:
- `frame/1` is for front facing image
- by default 32 frames to cover 360 view
- it's possible to pass params to get images of certain size

## WebP
WebP and WebM might be a better option than currently returned jpg from the api.

Browser support is good:
https://caniuse.com/webp

WebP has lossy and lossless, lossy might perform better

@todo: Check if Cylindo api can return webp

- Can we convert jpg to webP on the fly? e.g. react-image-webp

## WebM video 
It should be possible to render a webM video from the 32 images, set its thumbnail to the main front facing image (frame/1), and then control it via mouse drag and move.

Uncertainties:
- Is it faster than image switching? 
- If so, can we render webM video on the fly (client side, maybe service workers)? 
- How much would it impact browser performance?
- If client side performance is impacted too much, can we render webM via SSR?

## Possible solutions for mouse interaction

### Replacing image src via js

Most likely will have performance issues and glitching for users, especially if images aren't cached, so it's probably safe to dismiss this option.

## Image loading
Initially load (or even SSR) the frame 1.
Start loading the rest of images async, append to the DOM

### CSS z-index
Set z-index of frame 1 to `1`, the rest to `0` or `-1`
When user clicks and holds the mouse, get mouse position.
If still holding the mouse and moving, calculate distance from initial point of click
give an amount of pixels to 'scroll' 50% or 180deg direction
Divide that distance by 32/2 (half the images of 360deg view), or 16.
When mouse is moved in a given direction on x-axis, and 

## Mouse navigation ideas
- calculate mouse position to show relevant frame when clicking and dragging
- when mouse is clicked, trigger an invisible range slider which controls which image is in the view. possibly need to remap frames so that frame 1 is the middle value of range slider.
- (in case of WebM): navigate invisible video controls by using mousemove listener

## Performance considerations
Maybe if we calculate mouse acceleration in, we can skip some images (frames), as user's brain can probably make a connection anyway when they move the mouse fast. But maybe if it's lower than the alternative of 24fps, then it could be perceived as too much of a jump.

## PoC

Create a simple jpg viewer using scraped jpg images
As a first version scrollbar could be used instead of mouse down+drag to test performance without much logic
If performance is good, move on to using images from the API.

## Misc
 
- maybe an invisible element could be dragged instead of container using drag api instead of mouse move?
- add a dummy svg for responsive height
- useRef?
- canvas
- background-image: url()

## Ideas for the future

- Image sprites instead of 32 requests to the API
- WebM video with mousemove as controls to spin track back/forth
