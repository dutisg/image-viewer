import { useEffect, useRef, useState } from 'react';
import { addToFeatureArray, generateArrayFromRange } from '../helpers';

import {
  API_V2_URL,
  DEFAULT_FRAME,
  FRAME_COUNT,
  FRAME_INCREMENT_INTERVAL,
  GRAB_CURSOR_CSS_VALUE,
  GRABBING_CURSOR_CSS_VALUE,
  Z_INDEX_ACTIVE_FRAME,
  Z_INDEX_INACTIVE_FRAME,
} from '../vars';

const ImageViewer = () => {
  const arr = generateArrayFromRange(1, FRAME_COUNT);
  const viewerRef = useRef();

  const [mouseDown, setMouseDown] = useState(false);
  const [loading, setLoading] = useState(false);
  const [activeFrame, setActiveFrame] = useState(DEFAULT_FRAME);
  const [customerAndSku, setCustomerAndSku] = useState({
    customer: 4965, // 4404,
    sku: 'EMMA_ARMCHAIR', // 'ARCHIBALDCHAIR',
  });
  const [features, setFeatures] = useState([]);
  const [availableFeatures, setAvailableFeatures] = useState([]);

  const handleMouseDown = (e) => {
    setMouseDown(true);
  };

  const handleMouseUp = (e) => {
    setMouseDown(false);
  };

  const handleMouseMove = (e) => {
    const isMouseMovingOnAxisX = e.movementX !== 0;
    const isMouseMovingLeft = e.movementX < 0;

    // Ignore Y axis
    if (isMouseMovingOnAxisX) {
      // @TODO: solve - huge performance bottleneck. look into debounce.
      // Only fire every n amount pixels from where the mouse started.
      // if ((e.clientX - viewerOffsetLeft - mouseDownPositionX) % 8 === 0) {

      if (isMouseMovingLeft) {
        setActiveFrame((prev) => prev - FRAME_INCREMENT_INTERVAL);
      } else {
        setActiveFrame((prev) => prev + FRAME_INCREMENT_INTERVAL);
      }

      // }
    }
  };

  // @TODO: handle in a different way - fires on each mouse move
  const constructImageSrc = (customerAndSku, frameNumber, features) => {
    const featuresParams =
      features.length > 0 ? `&feature=${features.join('&feature=')}` : '';
    return `${API_V2_URL}${customerAndSku.customer}/products/${customerAndSku.sku}/frames/${frameNumber}/chair.jpg?size=560${featuresParams}`;
  };

  const handleChangeProduct = ({ customer, sku }) => {
    setFeatures([]);
    setCustomerAndSku({ customer, sku });
  };

  const handleAddFeature = (feature, option) => {
    setFeatures((prev) => {
      return addToFeatureArray(prev, `${feature}:${option}`);
    });
  };

  const getProductConfiguration = async () => {
    setLoading(true);
    const res = await fetch(
      `${API_V2_URL}${customerAndSku.customer}/products/${customerAndSku.sku}/configuration`
    );
    const json = await res.json();

    setAvailableFeatures(json.features);
    setLoading(false);
  };

  useEffect(() => {
    getProductConfiguration();
  }, [customerAndSku]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    // contain activeFrame within [0-32] range
    if (activeFrame > FRAME_COUNT) {
      setActiveFrame(DEFAULT_FRAME);
    }

    if (activeFrame < DEFAULT_FRAME) {
      setActiveFrame(FRAME_COUNT);
    }
  }, [activeFrame]);

  useEffect(() => {
    // Setup event listeners on document.
    // The reason why document is used instead of setting event on component, is
    // because we want to keep dragging the mouse when it is outside the
    // component bounding box or browser window.
    // We want to add these listeners if mouse is currently down.
    if (mouseDown) {
      document.addEventListener('mouseup', handleMouseUp);
      document.addEventListener('mousemove', handleMouseMove);
    }

    // Cleanup event listeners.
    return () => {
      document.removeEventListener('mouseup', handleMouseUp);
      document.removeEventListener('mousemove', handleMouseMove);
    };
  }, [mouseDown]);

  return (
    <>
      <div
        className="image-viewer"
        ref={viewerRef}
        onMouseDown={handleMouseDown}
        style={
          mouseDown
            ? { cursor: GRABBING_CURSOR_CSS_VALUE }
            : { cursor: GRAB_CURSOR_CSS_VALUE }
        }
      >
        {arr.map((item, index) => {
          return (
            <img
              key={item}
              style={
                item === activeFrame
                  ? { zIndex: Z_INDEX_ACTIVE_FRAME }
                  : { zIndex: Z_INDEX_INACTIVE_FRAME }
              }
              className="image"
              src={constructImageSrc(customerAndSku, index + 1, features)}
              alt="Furniture item frame"
            />
          );
        })}

        <div className="instructions-text">Drag to rotate</div>

        {loading && <div className="loading">Loading...</div>}

        {/* dummy image to make container responsive with absolutely positioned children */}
        {/* @TODO: make into a component*/}
        <svg viewBox="0 0 1 1" xmlns="http://www.w3.org/2000/svg" />
      </div>

      <div>
        <button
          onClick={() =>
            handleChangeProduct({ customer: 4965, sku: 'EMMA_ARMCHAIR' })
          }
        >
          EMMA_ARMCHAIR
        </button>
        <button
          onClick={() =>
            handleChangeProduct({ customer: 4404, sku: 'ARCHIBALDCHAIR' })
          }
        >
          ARCHIBALDCHAIR
        </button>

        {availableFeatures.map((feature) => (
          <div key={feature.code}>
            <p>{feature.code}</p>
            <ul>
              {feature.options.map(({ code }) => (
                <li key={code}>
                  <button onClick={() => handleAddFeature(feature.code, code)}>
                    {code}
                  </button>
                </li>
              ))}
            </ul>
          </div>
        ))}
      </div>
    </>
  );
};

export default ImageViewer;
