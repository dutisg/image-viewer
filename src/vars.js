export const API_V2_URL = 'https://content.cylindo.com/api/v2/';

export const FRAME_COUNT = 32;
export const Z_INDEX_INACTIVE_FRAME = 1;
export const Z_INDEX_ACTIVE_FRAME = 2;
export const DEFAULT_FRAME = 1;
export const FRAME_INCREMENT_INTERVAL = 1;
export const GRAB_CURSOR_CSS_VALUE = 'grab';
export const GRABBING_CURSOR_CSS_VALUE = 'grabbing';
