/**
 * Returns an array of numbers from 1 to N.
 *
 * @param {number} firstNumber
 * @param {number} lastNumber
 * @example
 *   // returns [1, 2, 3, 4, 5]
 *   generateArrayFromOneToN(5)
 * @return {number[]}
 */
export const generateArrayFromRange = (firstNumber, lastNumber) => {
  // @TODO: throw error if firstNumber is >= lastNumber
  return Array.from(Array(lastNumber + 1).keys()).slice(firstNumber);
};

/**
 * Replace colon separated items if the first word matches, otherwise push to
 * array. No duplicates allowed.
 * @param {Array} arr
 * @param {string} newValue - colon separated string
 * @example
 *   // returns ['TEST:123']
 *   addToFeatureArray([], 'TEST:123')
 *
 *   // returns ['TEST:222']
 *   addToFeatureArray(['TEST:123'], 'TEST:222')
 *
 *   // returns ['TEST:222']
 *   addToFeatureArray(['TEST:123'], 'TEST:222')
 *
 *   // returns ['TEST:222', 'BEST:123']
 *   addToFeatureArray(['TEST:222'], 'BEST:123')
 * @return {*}
 */
export const addToFeatureArray = (arr, newValue) => {
  // @TODO: refactor and write tests

  // array is empty
  if (arr.length === 0) {
    arr.push(newValue);
    return arr;
  }

  // item exists as a whole
  if (arr.indexOf(newValue) !== -1) {
    return arr;
  }

  const newArr = arr.map((item) => {
    const regex = new RegExp(newValue.split(':')[0]);

    if (regex.test(item)) {
      return newValue;
    } else {
      return item;
    }
  });

  if (newArr.indexOf(newValue) === -1) {
    newArr.push(newValue);
  }

  return newArr;
};
